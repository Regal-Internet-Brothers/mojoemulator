mojoemulator
============

A module which provides a standalone (Console/command-line) application-framework modeled after (And compatible with) [the official 'mojo' module](https://github.com/blitz-research/monkey/tree/develop/modules/mojo) for the [Monkey programming language](https://github.com/blitz-research/monkey).

Despite its name, this does not emulate anything; it's simply a bare-bones wrapper for some of the standard Mojo functionality. This is mainly used for game servers, as you can keep most of your design philosophies, instead of going with a completely different work-flow.
